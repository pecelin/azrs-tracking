#!/bin/bash

CONFIG_PATH=".clang-format"

SRC_DIRS="."

EXTENSIONS="cpp hpp c h"

for dir in $SRC_DIRS; do
    for ext in $EXTENSIONS; do
        find "$dir" -name "*.$ext" -exec clang-format -i -style=file -assume-filename="$CONFIG_PATH" {} \;
    done
done

echo "Formatting complete."