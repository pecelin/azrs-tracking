import gdb

class PrintContainerSize(gdb.Command):
    """Print the size of a C++ standard library container.

    Usage: print_container_size variable_name
    """

    def __init__(self):
        super(PrintContainerSize, self).__init__("print_container_size", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        argv = gdb.string_to_argv(arg)
        if len(argv) != 1:
            gdb.write("Usage: print_container_size variable_name\n")
            return

        variable_name = argv[0]
        try:
            container = gdb.parse_and_eval(variable_name)
            size = container['c'].invoke()
            gdb.write(f"Size of {variable_name}: {size}\n")
        except (gdb.error, RuntimeError) as e:
            gdb.write(f"Error accessing size of {variable_name}: {str(e)}\n")

PrintContainerSize()
